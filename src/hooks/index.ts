import type { Handle } from '@sveltejs/kit';

export const handle: Handle = ({ event, resolve }) => {
	let assign: any = {};

	// fuck facebook

	if (event.request.url.match(/\?fbclid=.+/)) {
		assign = {
			redirected: true,
			headers: {
				Location: event.request.url.replace(/[\?&]fbclid=[^&]+/, '')
			}
		};
	}

	let r = resolve(event);
	Object.assign(r, assign);
	return r;
};
