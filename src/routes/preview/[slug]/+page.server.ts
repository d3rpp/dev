import type { PageServerLoad } from './$types';
import { storyblokApi } from '$lib/utils';

export const load: PageServerLoad = async ({params}) => {
	const { data } = await storyblokApi.get(`cdn/stories/${params.slug}`, {
		version: 'draft'
	});

	return { story: data.story };
}
