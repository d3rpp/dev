import type { RequestHandler } from './$types';

const robots_txt = `
User-Agent: *
Allow /
Allow /blog/*

Disallow /preview/*
`.trim();

export const GET: RequestHandler = async (_a) => {
	return new Response(robots_txt);
};
