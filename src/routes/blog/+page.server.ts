import type { PageServerLoad } from './$types';
import { storyblokApi } from '$lib/utils';

interface StoryBlokStory {
	name: any;
	slug: any;
	content: { banner: { alt: any; filename: any } };
	created_at: any;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const load: PageServerLoad = async (_params) => {
	const { data } = await storyblokApi.get('cdn/stories', {
		sort_by: 'created_at:desc',
		excluding_fields: 'content,_editable,component,_uid,publish_date',
		version: 'published'
	});

	// @ts-ignore
	const output = data.stories.map((before: StoryBlokStory) => {
		return {
			name: before.name,
			slug: before.slug,
			content: {
				banner: {
					alt: before.content.banner.alt,
					filename: before.content.banner.filename
				}
			},
			created_at: before.created_at
		};
	});

	return { posts: output };
};
