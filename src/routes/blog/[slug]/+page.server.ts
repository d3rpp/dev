import type { PageServerLoad } from './$types';
// import { story } from '@storyblok/js';
import { storyblokApi } from '$lib/utils';

export const load: PageServerLoad = async ({ params }) => {
	// const storyblokApi = useStoryblokApi();
	const { data } = await storyblokApi.get(`cdn/stories/${params.slug}`, {
		version: 'published'
	});

	const before = data.story;

	const output = {
		name: before.name,
		created_at: before.created_at,
		content: {
			banner: {
				alt: before.content.banner.alt,
				filename: before.content.banner.filename
			},
			description: before.content.description,
			content: before.content.content
		},
		slug: before.slug
	};

	return { story: output };
};
