import { storyblokInit, apiPlugin } from '@storyblok/js';

export const local_date_string_options: Intl.DateTimeFormatOptions = {
	day: '2-digit',
	weekday: 'short',
	month: 'long',
	year: 'numeric'
};

export const { storyblokApi } = storyblokInit({
	accessToken: process.env.STORYBLOK_PREVIEW,
	use: [apiPlugin]
});

// this is weird, it works without issues, but the types arent setup right
