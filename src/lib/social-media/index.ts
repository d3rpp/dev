import FB from './facebook.svelte';
import GH from './github.svelte';
import GL from './gitlab.svelte';
import IG from './instagram.svelte';
import LI from './linkedin.svelte';
import ST from './steam.svelte';
import TW from './twitch.svelte';
import YT from './youtube.svelte';

export const icons: [typeof FB, string, string][] = [
	[FB, 'Facebook', 'https://www.facebook.com/d3rpp.dev'],
	[GH, 'GitHub', 'https://github.com/d3rpp'],
	[GL, 'GitLab', 'https://gitlab.com/d3rpp'],
	[IG, 'Instagram', 'https://instagram.com/d3rpp.dev'],
	[LI, 'LinkedIn', 'https://linkedin.com/in/hudson-curren'],
	[ST, 'Steam', 'https://steamcommunity.com/id/d3rpp_/'],
	[TW, 'Twitch', 'https://twitch.tv/d3rpp_dev'],
	[YT, 'YouTube', 'https://www.youtube.com/channel/UCczt_IK3fZWT0h0jdQRx6dA']
];
