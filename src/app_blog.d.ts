declare interface StoryblokPageWithPageContent {
	name: string;
	id: number;
	created_at: string;
	content: {
		banner: {
			alt: string;
			filename: string;
		};
		description: string;
		content: StoryDoc;
	};
	slug: string;
}

declare interface StoryblokPageWithoutPageContent {
	name: string;
	created_at: string;
	content: {
		banner: {
			alt?: string;
			filename: string;
		};
		description: string;
	};
	slug: string;
}

// declare a type called Blok that is a union type of everything under the StoryblokComponents namespace
declare type Blok =
	| StoryblokComponents.Paragraph
	| StoryblokComponents.Heading
	| StoryblokComponents.BulletList
	| StoryblokComponents.CodeBlock
	| StoryblokComponents.HorzontalRule
	| StoryblokComponents.Image
	| StoryblokComponents.ListItem
	| StoryblokComponents.OrderedList
	| StoryblokComponents.Text;

declare type Marks =
	| StoryblokMarks.Bold
	| StoryblokMarks.Italic
	| StoryblokMarks.Strikethrough
	| StoryblokMarks.Underline
	| StoryblokMarks.Link
	| StoryblokMarks.Code;

declare interface StoryDoc {
	type: 'doc';
	content: Blok[];
}

declare namespace StoryblokMarks {
	declare interface Bold {
		type: 'bold';
	}

	declare interface Italic {
		type: 'italic';
	}

	declare interface Strikethrough {
		type: 'strike';
	}

	declare interface Underline {
		type: 'underline';
	}

	declare interface Link {
		type: 'link';
		attrs: {
			href: string;
			uuid: unknown;
			anchor: unknown;
			target?: string;
			linktype: string;
		};
	}

	declare interface Code {
		type: 'code';
	}
}

declare namespace StoryblokComponents {
	declare interface Paragraph {
		type: 'paragraph';
		content: Blok[];
	}

	declare interface Heading {
		type: 'heading';
		attrs: {
			level: 1 | 2 | 3 | 4 | 5 | 6;
		};
		content: Blok[];
	}

	declare interface Text {
		type: 'text';
		text: string;
		marks?: Marks[];
	}

	declare interface BulletList {
		type: 'bullet_list';
		content: ListItem[];
	}

	declare interface OrderedList {
		type: 'ordered_list';
		content: ListItem[];
	}

	declare interface ListItem {
		type: 'list_item';
		content: Blok[];
	}

	declare interface HorzontalRule {
		type: 'horizontal_rule';
	}

	declare interface Image {
		type: 'image';
		attrs: {
			alt?: string;
			src: string;
			title?: string;
			copyright?: string;
		};
	}

	declare interface CodeBlock {
		type: 'code_block';
		attrs: {
			class: `language-${string}`;
		};
		content: Text[];
	}
}
