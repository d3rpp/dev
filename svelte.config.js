import adapter from '@sveltejs/adapter-vercel';
import preprocess from 'svelte-preprocess';

let hashes = {};

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess({
		scss: {
			prependData: '@import "src/_common.scss";'
		}
	}),

	kit: {
		adapter: adapter(),
		browser: {
			hydrate: true,
			router: true
		},
		csp: {
			mode: 'auto',
			directives: {
				'style-src': ['self', 'unsafe-inline'],
				'script-src': ['self'],
				'script-src-elem': [
					'self',
					'unsafe-inline',
					'https://app.storyblok.com'
				],
				'img-src': [
					'self',
					'https://cdn.ko-fi.com',
					'https://a.storyblok.com'
				],
				'connect-src':
					process.env.NODE_ENV == 'development'
						? ['self', 'https://api.storyblok.com', 'wss:']
						: ['self', 'https://api.storyblok.com'],
				'default-src': ['self']
			}
		},
		prerender: {
			default: false,
			enabled: true,
			onError: 'fail'
		},
		trailingSlash: 'always',
		serviceWorker: {
			register: false
		},
	},
	compilerOptions: {
		cssHash: ({ filename, css, hash }) => {
			let h = hash(filename + css);

			if (!hashes[h]) {
				hashes[h] = Object.keys(hashes).length.toString(16);
			}

			return `d${hashes[h]}`;
		}
	}
};

export default config;
