import { sveltekit } from '@sveltejs/kit/vite';

/** @type {import('vite').UserConfig} */
const config = {
	plugins: [sveltekit()],
	build: {
		rollupOptions: {
			treeshake: true
		}
	},
	define: {
		'process.env': process.env
	},
	server: {
		// https: true
	}
};

export default config;
